
#include "ct_ga.h"

////////////////////////////////////////////////////////////////////////////////
/// STATIC FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			init_population(t_ga_population *population) {
	size_t			i;

	for (i = 0; i < population->size; i++) {
		population->specimens[i].state = STATE_ALIVE;
		population->specimens[i].note = 0;
		population->specimens[i].content = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

t_ga_population		*ga_popnew(size_t size, uint8_t rate, uint8_t type) {
	size_t			to_alloc;
	t_ga_population	*new_population;

	to_alloc = sizeof(t_ga_population) + sizeof(t_ga_specimen) * size;
	if ((to_alloc - sizeof(t_ga_population)) / sizeof(t_ga_specimen) != size)
		return (NULL);
	new_population = malloc(to_alloc);
	if (new_population == NULL)
		return (NULL);
	new_population->size = size;
	new_population->selection_rate = rate;
	new_population->selection_type = type;
	new_population->f_death = NULL;
	new_population->f_birth = NULL;
	init_population(new_population);
	return (new_population);
}
