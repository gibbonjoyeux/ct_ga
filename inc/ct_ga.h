
#ifndef CT_GA_H
# define CT_GA_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

# include <inttypes.h>
# include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

# define STATE_ALIVE			1
# define STATE_DEAD				0
# define SELECTION_RANK			0
# define SELECTION_WHEEL		1
# define SELECTION_TOURNAMENT	2

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef struct s_ga_population	t_ga_population;
typedef struct s_ga_specimen	t_ga_specimen;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct				s_ga_specimen {
	uint8_t			state;
	uint8_t			note;
	void			*content;
};

struct				s_ga_population { 
	void			(*f_death)(void*);
	void			*(*f_birth)(size_t size, void**);
	uint8_t			selection_rate;
	uint8_t			selection_type;
	size_t			size;
	t_ga_specimen	specimens[0];
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////



#endif
